package com.dan.feignservice.fallback;

import com.dan.feignservice.UserCentrollerFeign;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class UserCenterFallBack implements FallbackFactory<UserCentrollerFeign> {

//    @Override
//    public String userTest() {
//        log.info("userTest 降级处理.....");
//        return null;
//    }

    @Override
    public UserCentrollerFeign create(Throwable throwable) {


        return new UserCentrollerFeign() {
            @Override
            public String userTest() {
                log.info("userTest 降级处理.....", throwable);
                return "userTest 降级处理.....";
            }
        };
    }


}
