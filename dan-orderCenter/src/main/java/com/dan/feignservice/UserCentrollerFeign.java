package com.dan.feignservice;

import com.dan.feignservice.fallback.UserCenterFallBack;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;

@Service
@FeignClient(name = "user-center", fallbackFactory = UserCenterFallBack.class)
public interface UserCentrollerFeign {

    @GetMapping("UserTest")
    String userTest();
}
