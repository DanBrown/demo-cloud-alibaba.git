package com.dan.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TypeEnums {
    AAA,
    BBB,
    CCC
}
