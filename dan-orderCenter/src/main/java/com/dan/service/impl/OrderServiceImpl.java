package com.dan.service.impl;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.dan.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class OrderServiceImpl implements OrderService {


    /**
     * @return hello world
     */
    @SentinelResource(value = "sayHello")
    @Override
    public String sayHello(String str) {

        System.out.println(str);
        return str;
    }
}
