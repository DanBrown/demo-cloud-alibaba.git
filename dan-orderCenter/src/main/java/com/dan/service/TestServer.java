package com.dan.service;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class TestServer {

    @SentinelResource(value = "commom", blockHandler = "testAFallback")
    public String commom(){
        log.info("common.....");
        return "commom";
    }


    public String testAFallback(BlockException ex) {
        return "ex testA";
    }
}
