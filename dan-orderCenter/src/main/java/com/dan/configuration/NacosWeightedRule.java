package com.dan.configuration;

/**
 * @Author 王志鹏
 * @Date 2021/2/11 11:44
 **/

import com.alibaba.cloud.nacos.NacosDiscoveryProperties;
import com.alibaba.cloud.nacos.ribbon.NacosServer;
import com.alibaba.nacos.api.exception.NacosException;
import com.alibaba.nacos.api.naming.NamingService;
import com.alibaba.nacos.api.naming.pojo.Instance;
import com.netflix.client.config.IClientConfig;
import com.netflix.loadbalancer.AbstractLoadBalancerRule;
import com.netflix.loadbalancer.BaseLoadBalancer;
import com.netflix.loadbalancer.Server;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

/**
 * 功能描述  Ribbon 配合使用 Nacos 的权重功能
 *
 * @author 王志鹏
 * @date 2021/2/11/11:44
 * @return
 */
@Configuration
@Slf4j
// implements IRule 能够编写自己的负载均衡算法
public class NacosWeightedRule extends AbstractLoadBalancerRule {

    @Autowired
    private NacosDiscoveryProperties nacosDiscoveryProperties;

    @Override
    public void initWithNiwsConfig(IClientConfig iClientConfig) {
        //读取配置文件并初始化
    }

    @Override
    public Server choose(Object key) {
        System.out.println("1111111111111");
//        ILoadBalancer loadBalancer = this.getLoadBalancer();    // Ribbon的入口
        BaseLoadBalancer baseLoadBalancer = (BaseLoadBalancer) this.getLoadBalancer();    // Ribbon的入口
        //想要请求微服务的名称
        String name = baseLoadBalancer.getName();

        //实现负载均衡算法

        //拿到服务发现的相关API
        NamingService namingService = nacosDiscoveryProperties.namingServiceInstance();
        Instance instance = null;
        try {
            instance = namingService.selectOneHealthyInstance(name);
        } catch (NacosException e) {
            e.printStackTrace();
        }
        log.info("选择的实例是:port = {}, instance = {}",instance.getPort(),instance);
        return new NacosServer(instance);
    }
}
