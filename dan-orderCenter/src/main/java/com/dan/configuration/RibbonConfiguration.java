package com.dan.configuration;

import com.netflix.loadbalancer.IRule;
import org.springframework.cloud.netflix.ribbon.RibbonClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author 王志鹏
 * @Date 2021/2/11 10:32
 **/
/**
*功能描述 全局配置 不建议使用java 代码方式配置，建议使用配置文件配置
* @author 王志鹏
* @date 2021/2/11/11:36

* @return
*/
//@Configuration
//@RibbonClients(defaultConfiguration = RibbonConfiguration.class)
public class RibbonConfiguration {
//    @Bean
//    public IRule ribbonRule(){
//        return new NacosWeightedRule();
//    }
}
