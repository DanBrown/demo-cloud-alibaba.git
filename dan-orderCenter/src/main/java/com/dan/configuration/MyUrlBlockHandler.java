package com.dan.configuration;

import com.alibaba.csp.sentinel.adapter.servlet.callback.UrlBlockHandler;
import com.alibaba.csp.sentinel.adapter.spring.webmvc.callback.BlockExceptionHandler;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.alibaba.csp.sentinel.slots.block.authority.AuthorityException;
import com.alibaba.csp.sentinel.slots.block.degrade.DegradeException;
import com.alibaba.csp.sentinel.slots.block.flow.FlowException;
import com.alibaba.csp.sentinel.slots.block.flow.param.ParamFlowException;
import com.alibaba.csp.sentinel.slots.system.SystemBlockException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.*;
import org.apache.commons.lang.CharEncoding;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
//public class MyUrlBlockHandler implements UrlBlockHandler {
public class MyUrlBlockHandler implements BlockExceptionHandler {
    /*@Override
    public void blocked(HttpServletRequest request, HttpServletResponse response, BlockException e) throws IOException {

        ErrorMsg errorMsg = null;
        if(e instanceof FlowException){
            errorMsg = ErrorMsg.builder().code(100).msg("限流了！").build();
        }else if(e instanceof DegradeException){
            errorMsg = ErrorMsg.builder().code(101).msg("降级了！").build();
        }else if(e instanceof ParamFlowException){
            errorMsg = ErrorMsg.builder().code(102).msg("热点参数限流！").build();
        }else if(e instanceof SystemBlockException){
            errorMsg = ErrorMsg.builder().code(103).msg("系统规则（负载/...不满足要求）！").build();
        }else if(e instanceof AuthorityException){
            errorMsg = ErrorMsg.builder().code(104).msg("授权规则不通过！！").build();
        }
        System.out.println("11111111111111111");
        response.setStatus(500);
        response.setCharacterEncoding(CharEncoding.UTF_8);
        response.setHeader("Content-Type","application/json;charset=utf-8");
        response.setContentType("application/json;charset=utf-8");
        new ObjectMapper().writeValue(response.getWriter(),errorMsg);
    }*/

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, BlockException e) throws Exception {
        ErrorMsg errorMsg = null;
        if(e instanceof FlowException){
            errorMsg = ErrorMsg.builder().code(100).msg("限流了！").build();
        }else if(e instanceof DegradeException){
            errorMsg = ErrorMsg.builder().code(101).msg("降级了！").build();
        }else if(e instanceof ParamFlowException){
            errorMsg = ErrorMsg.builder().code(102).msg("热点参数限流！").build();
        }else if(e instanceof SystemBlockException){
            errorMsg = ErrorMsg.builder().code(103).msg("系统规则（负载/...不满足要求）！").build();
        }else if(e instanceof AuthorityException){
            errorMsg = ErrorMsg.builder().code(104).msg("授权规则不通过！！").build();
        }
        response.setStatus(500);
        response.setCharacterEncoding(CharEncoding.UTF_8);
        response.setHeader("Content-Type","application/json;charset=utf-8");
        response.setContentType("application/json;charset=utf-8");
        new ObjectMapper().writeValue(response.getWriter(),errorMsg);
    }

    public static void main(String[] args)  {
        String url = "/shares/1";
        String[] split = url.split("/");
//        List<String> collect =
                String collect = Arrays.stream(split).map(str -> {
            System.out.printf(str+"kkk");
            if (NumberUtils.isCreatable(str)) {
                return "{number}";
            }
            return str;
        })
//                .collect(Collectors.toList());
             .reduce((a, b) -> {
                 System.out.println(a+"AAA");
                 System.out.println(b+"BBB");
                 String s =a + "/" + b;
                 return s;
             }).orElse("");
        System.out.println(collect);

        System.out.println("-----------华丽分割线---------");

        System.out.println("hello world");

        double a=77; double b=22;
        System.out.println(a/b);
        System.out.println(a%b);




    }

}
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
class ErrorMsg{
    private int code;
    private String msg;
}