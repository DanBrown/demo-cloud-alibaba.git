package com.dan.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.dan.enums.TypeEnums;
import com.dan.feignservice.UserCentrollerFeign;
import com.dan.service.OrderService;
import com.dan.service.TestServer;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/shares")
public class OrderController {

    @Autowired
    private UserCentrollerFeign userCentrollerFeign;

    @Autowired
    private OrderService orderService;

    @GetMapping("/{id}")
    public String findById(@PathVariable Integer id) {
        return "this.shareService.findById(id)";
    }


    @SentinelResource("hello")
    @GetMapping("orderTesta")
    public String orderTest() {
        final val aaa = TypeEnums.AAA;
        //sh startup.sh -m standalone
        return "I am OrderTest";
    }

    @GetMapping("feignTest")
    public String feignTest() {
        //sh startup.sh -m standalone
        return userCentrollerFeign.userTest();
    }

    @Autowired
    private TestServer testServer;

    @GetMapping("/testa")
    public String testa() {
        String testA = this.orderService.sayHello("testA");

        return testA;
    }

    @GetMapping("/testb")
    public String testb() {
        String testB = this.orderService.sayHello("testB");
        //sh startup.sh -m standalone

        return testB;
    }

}
