package com.dan.controller;


import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class UserController {

    @GetMapping("UserTest")
    public String UserTest() {
        log.info("我被请求了........");
        return "I am UserTest";
    }

}
